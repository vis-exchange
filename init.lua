-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

require("vis")
local vis = vis

local progname = ...

local marked_range = {}

vis.events.subscribe(vis.events.WIN_HIGHLIGHT, function(win)
	local range = marked_range[win.file]
	if range then
		win:style(win.STYLE_CURSOR, range.start, range.finish - 1)
	end
end)

local function exchange(file, range, pos)
	if not marked_range[file] then
		marked_range[file] = range
		return pos
	end
	local first, second, offset
	if range.start < marked_range[file].start then
		first, second = range, marked_range[file]
	else
		first, second = marked_range[file], range
		offset = (range.finish - range.start) - (marked_range[file].finish - marked_range[file].start)
	end
	if first.finish > second.start and first.finish < second.finish then
		-- if they overlap:
		return pos
	end
	local c1 = file:content(first)
	local c2 = file:content(second)
	local nl1 = c1:match("\n$")
	local nl2 = c2:match("\n$")
	-- If only one of the ranges is linewise, we need to insert extra newlines to make them both linewise.
	local linewise1 = nl1 ~= nl2 and nl1 or ""
	local linewise2 = nl1 ~= nl2 and nl2 or ""
	if first.finish >= second.finish then
		-- if one is fully contained in the other:
		file:delete(first)
		file:insert(first.start, linewise1 and (c2 .. linewise1) or c2)
		if marked_range[file].start <= range.start then
			-- if the marked one is larger:
			offset = marked_range[file].start - range.start
		end
	else
		file:delete(second)
		file:insert(second.start, (linewise1 or linewise2) and (linewise1 .. c1 .. linewise2) or c1)
		file:delete(first)
		file:insert(first.start, (linewise1 or linewise2) and (linewise2 .. c2 .. linewise1) or c2)
	end
	marked_range[file] = nil
	return range.start + (offset or 0)
end

local function exchange_clear(file, _, pos)
	marked_range[file] = nil
	return pos
end

local function operator(handler, no_range)
	local id = vis:operator_register(handler)
	return id >= 0 and function()
		vis:operator(id)
		if no_range then
			-- any motion would do:
			vis:motion(0)
		end
	end
end

vis.events.subscribe(vis.events.INIT, function()
	local function h(msg)
		return string.format("|@%s| %s", progname, msg)
	end
	local exchange_op = operator(exchange)
	vis:map(vis.modes.NORMAL, "cx", exchange_op, h"Exchange two ranges")
	vis:map(vis.modes.VISUAL, "X",  exchange_op, h"Exchange two ranges")
	vis:map(vis.modes.NORMAL, "cxx", "cxal")
	vis:map(vis.modes.NORMAL, "cxc", operator(exchange_clear, true), h"Clear range marked by `cx`")
end)
